const cart = [
    { article: 'Apple', count: 5, price: 1.5, discount: 0 },
    { article: 'Banana', count: 3, price: 0.75, discount: 10 },
    { article: 'Coco Nut', count: 2, price: 1.85, discount: 16 },
    { article: 'Date Plum', count: 7, price: 0.55, discount: 3 },
];

function toCurrency(amount, currency = 'USD') {
    return new Intl.NumberFormat('en', {
        style: 'currency',
        currency: currency
    }).format(amount);
}

function populateRow(product) {
    const cost = product.count * product.price * (1 - product.discount / 100);
    const tr = document.createElement('tr');
    tr.innerHTML = `
        <td>${product.article}</td> 
        <td>${product.count}</td> 
        <td>${toCurrency(product.price)}</td> 
        <td>${product.discount}%</td> 
        <td>${toCurrency(cost)}</td> 
    `.replace(/[\s\n\r]+/, '');
    return [tr, cost];
}

function populate(tblBody, articles = cart) {
    let sumCost = 0;
    articles.forEach(a => {
        const [tr, cost] = populateRow(a);
        tblBody.appendChild(tr);
        sumCost += cost;
    });

    document.querySelector('#subtotal').innerText = toCurrency(sumCost);

    const vatPercent = 25;
    document.querySelector('#vatPercent').innerText = `${vatPercent}%`;
    document.querySelector('#vatAmount').innerText = toCurrency(sumCost * vatPercent / 100);
    document.querySelector('#total').innerText = toCurrency(sumCost * (1 + vatPercent / 100));
}

populate(document.querySelector('tbody'));
