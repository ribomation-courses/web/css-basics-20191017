
const showBtn = document.querySelector('.show');
showBtn.addEventListener('click', ev => {
    ev.preventDefault();
    const pane = document.querySelector('.msg');
    //pane.classList.toggle('msg--show');

    pane.classList.add('msg--show');
    setTimeout(() => {
        pane.classList.remove('msg--show');
    }, 2000);
});
